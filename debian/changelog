php-horde-icalendar (2.1.8-7) unstable; urgency=medium

  * Team upload.
  * d/salsa-ci.yml: use aptly, simplify.
  * d/changelog: wrap long lines in changelog entries: 2.1.8-6.
  * d/control: Update standards version to 4.6.2, no changes needed.
  * d/control: Switch BD from pear-horde-channel to pear-channels.

 -- Anton Gladky <gladk@debian.org>  Thu, 05 Jan 2023 06:58:12 +0100

php-horde-icalendar (2.1.8-6) unstable; urgency=medium

  * d/t/control: Allow output to stderr for now. PHP 8.1 deprecated
    gmstrftime(), this needs upstream work.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 13 Jul 2022 16:02:07 +0200

php-horde-icalendar (2.1.8-5) unstable; urgency=medium

  * d/patches: Add debian/patches/1010_phpunit-8.x+9.x.patch.
    Fix tests with PHPUnit 8.x/9.x.
  * d/t/control: Require php-horde-test (>= 2.6.4+debian0-5~).

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 21 Jul 2020 22:20:52 +0000

php-horde-icalendar (2.1.8-4) unstable; urgency=medium

  [ Mike Gabriel ]
  * d/tests/control: Stop using deprecated needs-recommends restriction.
  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.

  [ Juri Grabowski ]
  * d/salsa-ci.yml: enable aptly

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 10:44:32 +0200

php-horde-icalendar (2.1.8-3) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959259).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/copyright: Update copyright attributions.
  * d/upstream/metadata: Add file. Comply with DEP-12.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 04 May 2020 21:26:34 +0200

php-horde-icalendar (2.1.8-2) unstable; urgency=medium

  * Bump debhelper from old 11 to 12.
  * salsa-ci.yml: Allow autopkgtest failure
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 19:56:40 +0200

php-horde-icalendar (2.1.8-1) unstable; urgency=medium

  * New upstream version 2.1.8

 -- Mathieu Parent <sathieu@debian.org>  Wed, 24 Oct 2018 22:20:50 +0200

php-horde-icalendar (2.1.7-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 May 2018 00:42:26 +0200

php-horde-icalendar (2.1.7-2) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Thu, 05 Apr 2018 12:03:05 +0200

php-horde-icalendar (2.1.7-1) unstable; urgency=medium

  * New upstream version 2.1.7

 -- Mathieu Parent <sathieu@debian.org>  Tue, 01 Aug 2017 21:59:23 +0200

php-horde-icalendar (2.1.6-1) unstable; urgency=medium

  * New upstream version 2.1.6

 -- Mathieu Parent <sathieu@debian.org>  Fri, 30 Jun 2017 21:33:33 +0200

php-horde-icalendar (2.1.4-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Tue, 07 Jun 2016 23:23:56 +0200

php-horde-icalendar (2.1.4-1) unstable; urgency=medium

  * New upstream version 2.1.4

 -- Mathieu Parent <sathieu@debian.org>  Sat, 26 Mar 2016 10:51:37 +0100

php-horde-icalendar (2.1.3-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition
  * Replace php5-* by php-* in d/tests/control

 -- Mathieu Parent <sathieu@debian.org>  Sat, 12 Mar 2016 23:26:51 +0100

php-horde-icalendar (2.1.3-1) unstable; urgency=medium

  * New upstream version 2.1.3

 -- Mathieu Parent <sathieu@debian.org>  Wed, 03 Feb 2016 21:16:39 +0100

php-horde-icalendar (2.1.2-1) unstable; urgency=medium

  * New upstream version 2.1.2

 -- Mathieu Parent <sathieu@debian.org>  Wed, 09 Dec 2015 21:45:32 +0100

php-horde-icalendar (2.1.1-3) unstable; urgency=medium

  * Upgaded to debhelper compat 9

 -- Mathieu Parent <sathieu@debian.org>  Sat, 24 Oct 2015 15:35:19 +0200

php-horde-icalendar (2.1.1-2) unstable; urgency=medium

  * Remove XS-Testsuite header in d/control
  * Update gbp.conf

 -- Mathieu Parent <sathieu@debian.org>  Sun, 09 Aug 2015 22:40:19 +0200

php-horde-icalendar (2.1.1-1) unstable; urgency=medium

  * New upstream version 2.1.1

 -- Mathieu Parent <sathieu@debian.org>  Wed, 29 Jul 2015 06:23:08 +0200

php-horde-icalendar (2.1.0-1) unstable; urgency=medium

  * New upstream version 2.1.0

 -- Mathieu Parent <sathieu@debian.org>  Thu, 18 Jun 2015 16:19:56 +0200

php-horde-icalendar (2.0.11-1) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change
  * New upstream version 2.0.11

 -- Mathieu Parent <sathieu@debian.org>  Mon, 04 May 2015 20:48:40 +0200

php-horde-icalendar (2.0.9-4) unstable; urgency=medium

  * Fixed DEP-8 tests, by removing "set -x"

 -- Mathieu Parent <sathieu@debian.org>  Sat, 11 Oct 2014 12:33:09 +0200

php-horde-icalendar (2.0.9-3) unstable; urgency=medium

  * Fixed DEP-8 tests

 -- Mathieu Parent <sathieu@debian.org>  Sat, 13 Sep 2014 11:19:14 +0200

php-horde-icalendar (2.0.9-2) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb
  * Add dep-8 (automatic as-installed package testing)

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Aug 2014 20:27:56 +0200

php-horde-icalendar (2.0.9-1) unstable; urgency=medium

  * New upstream version 2.0.9

 -- Mathieu Parent <sathieu@debian.org>  Thu, 05 Jun 2014 20:31:26 +0200

php-horde-icalendar (2.0.8-1) unstable; urgency=medium

  * New upstream version 2.0.8

 -- Mathieu Parent <sathieu@debian.org>  Sat, 17 May 2014 12:39:05 +0200

php-horde-icalendar (2.0.7-1) unstable; urgency=low

  * New upstream version 2.0.7

 -- Mathieu Parent <sathieu@debian.org>  Sun, 11 Aug 2013 12:33:46 +0200

php-horde-icalendar (2.0.6-1) unstable; urgency=low

  * New upstream version 2.0.6

 -- Mathieu Parent <sathieu@debian.org>  Mon, 24 Jun 2013 10:54:25 +0200

php-horde-icalendar (2.0.5-2) unstable; urgency=low

  * Use pristine-tar

 -- Mathieu Parent <sathieu@debian.org>  Wed, 05 Jun 2013 20:39:15 +0200

php-horde-icalendar (2.0.5-1) unstable; urgency=low

  * New upstream version 2.0.5

 -- Mathieu Parent <sathieu@debian.org>  Tue, 14 May 2013 20:46:43 +0200

php-horde-icalendar (2.0.4-1) unstable; urgency=low

  * New upstream version 2.0.4

 -- Mathieu Parent <sathieu@debian.org>  Sun, 07 Apr 2013 16:17:25 +0200

php-horde-icalendar (2.0.2-1) unstable; urgency=low

  * New upstream version 2.0.2

 -- Mathieu Parent <sathieu@debian.org>  Thu, 10 Jan 2013 20:25:09 +0100

php-horde-icalendar (2.0.1-2) unstable; urgency=low

  * Add a description of Horde in long description
  * Updated Standards-Version to 3.9.4, no changes
  * Replace horde4 by PEAR in git reporitory path
  * Fix Horde Homepage
  * Remove debian/pearrc, not needed with latest php-horde-role

 -- Mathieu Parent <sathieu@debian.org>  Wed, 09 Jan 2013 20:48:16 +0100

php-horde-icalendar (2.0.1-1) unstable; urgency=low

  * New upstream version 2.0.1
  * Fixed watchfile
  * Updated Standards-Version to 3.9.3: no change
  * Updated copyright format URL
  * Updated debian/pearrc to install Horde apps in /usr/share/horde
    instead of /usr/share/horde4
  * Updated Homepage
  * Added Vcs-* fields

 -- Mathieu Parent <sathieu@debian.org>  Fri, 30 Nov 2012 21:25:06 +0100

php-horde-icalendar (1.1.0-1) unstable; urgency=low

  * Horde_Icalendar package.
  * Initial packaging (Closes: #635795)
  * Copyright file by Soren Stoutner and Jay Barksdale

 -- Mathieu Parent <sathieu@debian.org>  Tue, 17 Jan 2012 18:44:56 +0100
